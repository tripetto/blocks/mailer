/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    HeadlessBlock,
    Slots,
    assert,
    tripetto,
    validator,
} from "@tripetto/runner";
import { IMailer } from "./interface";
import { IS_EMAIL } from "./regex";
import "./condition";

@tripetto({
    type: "headless",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
})
export class Mailer extends HeadlessBlock<IMailer> {
    readonly recipientSlot = assert(
        this.valueOf<string, Slots.String>("recipient")
    );
    readonly subjectSlot = assert(
        this.valueOf<string, Slots.String>("subject")
    );
    readonly messageSlot = assert(
        this.valueOf<string, Slots.String>("message")
    );
    readonly senderSlot = this.valueOf<string, Slots.String>("sender");
    readonly dataSlot = this.valueOf<boolean, Slots.Boolean>("data");

    do(): void {
        this.recipientSlot.value =
            (this.props.recipient && this.props.recipient !== "fixed"
                ? this.variableFor(this.props.recipient)?.string
                : this.props.recipientFixed) || "";
        this.subjectSlot.value = this.parseVariables(this.node.name || "", "-");
        this.messageSlot.value = this.parseVariables(
            this.node.description || "",
            "-",
            true
        );

        if (this.senderSlot) {
            const sender =
                (this.props.sender && this.props.sender !== "fixed"
                    ? this.variableFor(this.props.sender)?.string
                    : this.props.senderFixed) || "";

            this.senderSlot.value = (IS_EMAIL.test(sender) && sender) || "";
        }

        if (this.dataSlot && this.props.includeData) {
            this.dataSlot.value = true;
        }
    }

    @validator
    validate(): boolean {
        return (
            !this.recipientSlot.string ||
            IS_EMAIL.test(this.recipientSlot.string)
        );
    }
}
